package com.test.toyrobot

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.view.inputmethod.EditorInfo
import com.test.toyrobot.domain.Robot
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    val robot = Robot()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initializeUI()
    }

    private fun initializeUI() {
        buttonPlace.setOnClickListener {
            place()
        }
        buttonTurnLeft.setOnClickListener {
            executeCommand(Robot.COMMAND_TURN_LEFT)
        }
        buttonTurnRight.setOnClickListener {
            executeCommand(Robot.COMMAND_TURN_RIGHT)
        }
        buttonMove.setOnClickListener {
            executeCommand(Robot.COMMAND_MOVE)
        }
        buttonReport.setOnClickListener {
            executeCommand(Robot.COMMAND_REPORT)
        }
        textInputF.setOnEditorActionListener { textView, actionId, keyEvent ->
            if(actionId == EditorInfo.IME_ACTION_GO) {
                place()
                true
            } else {
                false
            }
        }
    }

    private fun place() {
        executeCommand("${Robot.COMMAND_PLACE} ${textInputX.text},${textInputY.text},${textInputF.text}")
    }

    private fun executeCommand(command: String) {
        var response = robot.executeCommand(command)

        if(response.position != null) {
            textViewResult.text = Html.fromHtml(getString(R.string.response_format_with_position,
                    response.success,
                    response.message,
                    response.position!!.x,
                    response.position!!.y,
                    response.position!!.f.name.toLowerCase()
            ))

        } else {
            textViewResult.text = Html.fromHtml(getString(R.string.response_format,
                    response.success,
                    response.message
            ))

        }

    }



}
