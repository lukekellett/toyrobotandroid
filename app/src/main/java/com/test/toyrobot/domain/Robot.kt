package com.test.toyrobot.domain

import com.test.toyrobot.domain.model.CardinalDirection
import com.test.toyrobot.domain.model.Position
import com.test.toyrobot.domain.model.RobotCommandResponse
import com.test.toyrobot.domain.model.TableSize
import com.test.toyrobot.domain.util.PositionUtil
import com.test.toyrobot.domain.util.toCardinalDirectionOrNull


class Robot(val tableSize: TableSize = TableSize(5, 5)) {

    companion object {
        val COMMAND_PLACE = "PLACE"
        val COMMAND_MOVE = "MOVE"
        val COMMAND_TURN_LEFT = "LEFT"
        val COMMAND_TURN_RIGHT = "RIGHT"
        val COMMAND_REPORT = "REPORT"

    }

    var position: Position? = null
        private set

    // Ideally a command would be a data object with a name and a dictionary of arguments, but to
    // keep with requirements it's a parsed string.
    fun executeCommand(command: String): RobotCommandResponse {

        var commandWithoutParameters = command
        var commandParameters: List<String>? = null

        if(command.indexOf(' ') > -1) {
            commandWithoutParameters = command.substring(0, command.indexOf(' '))
            commandParameters = command.substring(command.indexOf(' ') + 1).split(",").map { it.trim() }
        }

        commandWithoutParameters = commandWithoutParameters.toUpperCase()

        if(commandWithoutParameters != COMMAND_PLACE && position == null) {
            return RobotCommandResponse(false, "A 'PLACE' command must be issued first before '$commandWithoutParameters'.")
        }


        when(commandWithoutParameters) {
            COMMAND_PLACE -> return place(command, commandParameters)
            COMMAND_MOVE-> return move()
            COMMAND_TURN_LEFT -> return turnLeft()
            COMMAND_TURN_RIGHT -> return turnRight()
            COMMAND_REPORT -> return report()
            else -> return RobotCommandResponse(false, "Invalid command: '$commandWithoutParameters'")

        }
    }

    private fun place(command: String, commandParameters: List<String>?): RobotCommandResponse {
        if(commandParameters == null) {
            return RobotCommandResponse(false, "Invalid command: '$command'. Expected X,Y,F (e.g. PLACE 1,2,N) position parameters.")
        }

        if(commandParameters.size != 3) {
            return RobotCommandResponse(false, "Invalid command: '$command'. Expected 3 parameters (e.g. PLACE 1,2,N).")
        }

        var x: Int? = commandParameters.get(0).toIntOrNull()
        var y: Int? = commandParameters.get(1).toIntOrNull()
        var f: CardinalDirection? = commandParameters.get(2).toCardinalDirectionOrNull()

        if(x == null || y == null || f == null) {
            return RobotCommandResponse(false, "Invalid command parameters: '$command'. Expected X,Y,F (e.g. PLACE 1,2,N) position parameters.")
        }

        return place(x!!, y!!, f!!)

    }

    private fun place(x: Int, y: Int, f: CardinalDirection): RobotCommandResponse {
        var newPosition = PositionUtil.place(tableSize, Position(x, y, f))

        if(newPosition != null) {
            position = newPosition
            return RobotCommandResponse(position = position!!)
        } else {
            return RobotCommandResponse(false)
        }

    }

    private fun move(): RobotCommandResponse {
        var newPosition = PositionUtil.move(tableSize, position!!)
        if(newPosition != null) {
            position = newPosition
            return RobotCommandResponse(position = position!!)
        } else {
            return RobotCommandResponse(false, "Cannot move ${position!!.f.name.toLowerCase()} it would place the robot outside the table bounds.")
        }
    }

    private fun turnLeft(): RobotCommandResponse {
        position = PositionUtil.turnLeft(position!!)
        return RobotCommandResponse(position = position!!)
    }

    private fun turnRight(): RobotCommandResponse {
        position = PositionUtil.turnRight(position!!)
        return RobotCommandResponse(position = position!!)
    }

    private fun report(): RobotCommandResponse {
        return RobotCommandResponse(position = position!!)
    }

}