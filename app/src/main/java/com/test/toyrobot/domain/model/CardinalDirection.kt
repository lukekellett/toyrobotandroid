package com.test.toyrobot.domain.model


enum class CardinalDirection(){
    NORTH,
    EAST,
    SOUTH,
    WEST;

    fun next(): CardinalDirection {
        if(ordinal == CardinalDirection.values().last().ordinal) {
            return from(0)
        } else {
            return from(ordinal + 1)
        }

    }

    fun previous(): CardinalDirection {
        if(ordinal == CardinalDirection.values().first().ordinal) {
            return from(CardinalDirection.values().last().ordinal)
        } else {
            return from(ordinal - 1)
        }
    }

    companion object {
        fun from(findOrdinal: Int): CardinalDirection = CardinalDirection.values().first { it.ordinal == findOrdinal }
    }

}