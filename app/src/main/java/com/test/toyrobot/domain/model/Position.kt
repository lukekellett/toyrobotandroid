package com.test.toyrobot.domain.model


class Position(val x: Int, val y: Int, val f: CardinalDirection) {

    override fun equals(other: Any?): Boolean {
        if(other == null) {
            return false
        }

        if(other is Position) {
            return this.x == other.x && this.y == other.y && this.f == other.f
        }

        return false

    }

}