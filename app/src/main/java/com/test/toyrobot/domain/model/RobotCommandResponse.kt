package com.test.toyrobot.domain.model


class RobotCommandResponse(val success: Boolean = true, val message: String = if(success) "Success" else "Failure", var position: Position? = null) {

    override fun equals(other: Any?): Boolean {
        if(other == null) {
            return false
        }

        if(other is RobotCommandResponse) {
            return success == other.success && message == other.message && position == other.position
        }

        return false

    }

}