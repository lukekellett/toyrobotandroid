package com.test.toyrobot.domain.model


class TableSize(val x: Int, val y: Int) {

    override fun equals(other: Any?): Boolean {
        if(other == null) {
            return false
        }

        if(other is TableSize) {
            return this.x == other.x && this.y == other.y
        }

        return false

    }

}