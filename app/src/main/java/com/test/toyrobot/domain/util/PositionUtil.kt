package com.test.toyrobot.domain.util

import com.test.toyrobot.domain.model.CardinalDirection
import com.test.toyrobot.domain.model.Position
import com.test.toyrobot.domain.model.TableSize


object PositionUtil {

    fun place(tableSize: TableSize, position: Position): Position? {
        if(position.x < 0 || position.x > tableSize.x - 1 || position.y < 0 || position.y > tableSize.y - 1) {
            return null
        } else {
            return position
        }
    }

    fun move(tableSize: TableSize, position: Position): Position? {
        // Determine what dimension we are to move on
        val moveX = position.f == CardinalDirection.EAST || position.f == CardinalDirection.WEST
        val moveBy = if(position.f == CardinalDirection.NORTH || position.f == CardinalDirection.EAST) 1 else -1

        if(moveX) {
            val newX = position.x + moveBy

            if(newX < 0 || newX > tableSize.x - 1) {
                return null
            }

            return Position(newX, position.y, position.f)

        } else {
            val newY = position.y + moveBy

            if(newY < 0 || newY > tableSize.y - 1) {
                return null
            }

            return Position(position.x, newY, position.f)

        }

    }

    fun turnLeft(position: Position): Position {
        return Position(position.x, position.y, position.f.previous())
    }

    fun turnRight(position: Position): Position {
        return Position(position.x, position.y, position.f.next())
    }

}