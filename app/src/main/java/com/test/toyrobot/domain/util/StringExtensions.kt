package com.test.toyrobot.domain.util

import com.test.toyrobot.domain.model.CardinalDirection


fun String.toCardinalDirectionOrNull(): CardinalDirection? {
    when(this.toUpperCase()) {
        "N", "NORTH" -> return CardinalDirection.NORTH
        "E", "EAST" -> return CardinalDirection.EAST
        "S", "SOUTH" -> return CardinalDirection.SOUTH
        "W", "WEST" -> return CardinalDirection.WEST
        else -> return null
    }
}