package com.test.toyrobot

import com.test.toyrobot.domain.Robot
import com.test.toyrobot.domain.model.CardinalDirection
import com.test.toyrobot.domain.model.Position
import com.test.toyrobot.domain.model.RobotCommandResponse
import org.junit.Test

import org.junit.Assert.*

class RobotTest {

    @Test
    fun commands_unplaced_returnFailure() {
        var robot = Robot()
        assertFalse(robot.executeCommand(Robot.COMMAND_REPORT).success)
        assertFalse(robot.executeCommand(Robot.COMMAND_MOVE).success)
        assertFalse(robot.executeCommand(Robot.COMMAND_TURN_LEFT).success)
        assertFalse(robot.executeCommand(Robot.COMMAND_TURN_RIGHT).success)

    }

    @Test
    fun commandReport_placed_returnsSuccess() {
        var robot = Robot()
        robot.executeCommand("${Robot.COMMAND_PLACE} 0,2,N")
        assertEquals(RobotCommandResponse(position = Position(0, 2, CardinalDirection.NORTH)), robot.executeCommand(Robot.COMMAND_REPORT))

    }

    @Test
    fun commandTurnLeft_placedFacingEast_returnsSuccessFacingNorth() {
        var robot = Robot()
        robot.executeCommand("${Robot.COMMAND_PLACE} 0,2,E")
        assertEquals(RobotCommandResponse(position = Position(0, 2, CardinalDirection.NORTH)), robot.executeCommand(Robot.COMMAND_TURN_LEFT))

    }

    @Test
    fun commandTurnRight_placedFacingEast_returnsSuccessFacingSouth() {
        var robot = Robot()
        robot.executeCommand("${Robot.COMMAND_PLACE} 0,2,E")
        robot.executeCommand(Robot.COMMAND_TURN_RIGHT)
        assertEquals(RobotCommandResponse(position = Position(0, 2, CardinalDirection.SOUTH)), robot.executeCommand(Robot.COMMAND_REPORT))

    }

    @Test
    fun commandPlace_invalidPositions_allFail() {
        var robot = Robot()
        assertFalse(robot.executeCommand("${Robot.COMMAND_PLACE} 0,-1,E").success)
        assertFalse(robot.executeCommand("${Robot.COMMAND_PLACE} 0,1,G").success)
        assertFalse(robot.executeCommand("${Robot.COMMAND_PLACE} ").success)
        assertFalse(robot.executeCommand("${Robot.COMMAND_PLACE} 0").success)
        assertFalse(robot.executeCommand("${Robot.COMMAND_PLACE} -1,0,E").success)
        assertFalse(robot.executeCommand("${Robot.COMMAND_PLACE} 10,1,E").success)
        assertFalse(robot.executeCommand("${Robot.COMMAND_PLACE} 0,10,E").success)

    }

}
