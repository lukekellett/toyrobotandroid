package com.test.toyrobot.domain.model

import org.junit.*
import org.junit.Test
import org.junit.Assert.*

class CardinalDirectionTest {

    @Test
    fun from_withValidInput_returnsExpected() {
        assertEquals(CardinalDirection.NORTH, CardinalDirection.from(0))
        assertEquals(CardinalDirection.EAST, CardinalDirection.from(1))
        assertEquals(CardinalDirection.SOUTH, CardinalDirection.from(2))
        assertEquals(CardinalDirection.WEST, CardinalDirection.from(3))
    }

    @Test
    fun from_withInvalidInput_throwsException() {
        try {
            CardinalDirection.from(-1)
        } catch (exception: NoSuchElementException) {
            assertTrue(true)
        } catch (exception: Exception) {
            fail()
        }
    }

    @Test
    fun next_returnsNextDirectionRecursively() {
        var direction = CardinalDirection.NORTH
        direction = direction.next()
        assertEquals(CardinalDirection.EAST, direction)
        direction = direction.next()
        assertEquals(CardinalDirection.SOUTH, direction)
        direction = direction.next()
        assertEquals(CardinalDirection.WEST, direction)
        direction = direction.next()
        assertEquals(CardinalDirection.NORTH, direction)

    }

    @Test
    fun next_returnsPreviousDirectionRecursively() {
        var direction = CardinalDirection.NORTH
        direction = direction.previous()
        assertEquals(CardinalDirection.WEST, direction)
        direction = direction.previous()
        assertEquals(CardinalDirection.SOUTH, direction)
        direction = direction.previous()
        assertEquals(CardinalDirection.EAST, direction)
        direction = direction.previous()
        assertEquals(CardinalDirection.NORTH, direction)

    }

}