package com.test.toyrobot.domain.model

import org.junit.Assert
import org.junit.Test

class RobotCommandResponseTest {

    @Test
    fun constructor_success_returnsSuccessfulMessage() {
        Assert.assertEquals("Success", RobotCommandResponse(true).message)
        Assert.assertEquals("Gibberish", RobotCommandResponse(true, "Gibberish").message)
    }

    @Test
    fun constructor_failure_returnsFailureMessage() {
        Assert.assertEquals("Failure", RobotCommandResponse(false).message)
        Assert.assertEquals("Gibberish", RobotCommandResponse(false, "Gibberish").message)
    }

}