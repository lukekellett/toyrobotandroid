package com.test.toyrobot.model.util

import com.test.toyrobot.domain.model.CardinalDirection
import com.test.toyrobot.domain.model.Position
import com.test.toyrobot.domain.model.TableSize
import com.test.toyrobot.domain.util.PositionUtil
import org.junit.Assert.*
import org.junit.Test

class PositionUtilTest {

    @Test
    fun place_returnsExpected() {
        assertEquals(Position(0, 0, CardinalDirection.NORTH), PositionUtil.place(TableSize(2, 2), Position(0, 0, CardinalDirection.NORTH)))
        assertEquals(Position(1, 1, CardinalDirection.SOUTH), PositionUtil.place(TableSize(2, 2), Position(1, 1, CardinalDirection.SOUTH)))
        assertEquals(Position(0, 0, CardinalDirection.EAST), PositionUtil.place(TableSize(2, 2), Position(0, 0, CardinalDirection.EAST)))
        assertEquals(Position(1, 1, CardinalDirection.WEST), PositionUtil.place(TableSize(2, 2), Position(1, 1, CardinalDirection.WEST)))
        assertEquals(null, PositionUtil.place(TableSize(2, 2), Position(-1, 1, CardinalDirection.NORTH)))
        assertEquals(null, PositionUtil.place(TableSize(2, 2), Position(0, -1, CardinalDirection.NORTH)))
        assertEquals(null, PositionUtil.place(TableSize(2, 2), Position(2, 1, CardinalDirection.NORTH)))
        assertEquals(null, PositionUtil.place(TableSize(2, 2), Position(0, 2, CardinalDirection.NORTH)))
    }

    @Test
    fun move_returnsExpected() {
        assertEquals(Position(0, 1, CardinalDirection.NORTH), PositionUtil.move(TableSize(5, 5), Position(0, 0, CardinalDirection.NORTH)))
        assertEquals(Position(0, 2, CardinalDirection.NORTH), PositionUtil.move(TableSize(5, 5), Position(0, 1, CardinalDirection.NORTH)))
        assertEquals(null, PositionUtil.move(TableSize(5, 5), Position(0, 4, CardinalDirection.NORTH)))

        assertEquals(Position(1, 0, CardinalDirection.EAST), PositionUtil.move(TableSize(5, 5), Position(0, 0, CardinalDirection.EAST)))
        assertEquals(Position(2, 0, CardinalDirection.EAST), PositionUtil.move(TableSize(5, 5), Position(1, 0, CardinalDirection.EAST)))
        assertEquals(null, PositionUtil.move(TableSize(5, 5), Position(4, 0, CardinalDirection.EAST)))

        assertEquals(Position(0, 3, CardinalDirection.SOUTH), PositionUtil.move(TableSize(5, 5), Position(0, 4, CardinalDirection.SOUTH)))
        assertEquals(Position(0, 0, CardinalDirection.SOUTH), PositionUtil.move(TableSize(5, 5), Position(0, 1, CardinalDirection.SOUTH)))
        assertEquals(null, PositionUtil.move(TableSize(5, 5), Position(0, 0, CardinalDirection.SOUTH)))

        assertEquals(Position(3, 0, CardinalDirection.WEST), PositionUtil.move(TableSize(5, 5), Position(4, 0, CardinalDirection.WEST)))
        assertEquals(Position(0, 0, CardinalDirection.WEST), PositionUtil.move(TableSize(5, 5), Position(1, 0, CardinalDirection.WEST)))
        assertEquals(null, PositionUtil.move(TableSize(5, 5), Position(0, 0, CardinalDirection.WEST)))

    }

    @Test
    fun turnLeft_turnsLeftRecursively() {
        var position = Position(0,0, CardinalDirection.NORTH)
        assertEquals(position.f, CardinalDirection.NORTH)

        position = PositionUtil.turnLeft(position)
        assertEquals(position.f, CardinalDirection.WEST)

        position = PositionUtil.turnLeft(position)
        assertEquals(position.f, CardinalDirection.SOUTH)

        position = PositionUtil.turnLeft(position)
        assertEquals(position.f, CardinalDirection.EAST)

        position = PositionUtil.turnLeft(position)
        assertEquals(position.f, CardinalDirection.NORTH)

    }

    @Test
    fun turnRight_turnsRightRecursively() {
        var position = Position(0,0, CardinalDirection.NORTH)
        assertEquals(position.f, CardinalDirection.NORTH)

        position = PositionUtil.turnRight(position)
        assertEquals(position.f, CardinalDirection.EAST)

        position = PositionUtil.turnRight(position)
        assertEquals(position.f, CardinalDirection.SOUTH)

        position = PositionUtil.turnRight(position)
        assertEquals(position.f, CardinalDirection.WEST)

        position = PositionUtil.turnRight(position)
        assertEquals(position.f, CardinalDirection.NORTH)

    }
}