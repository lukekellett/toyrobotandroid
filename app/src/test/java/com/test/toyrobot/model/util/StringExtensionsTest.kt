package com.test.toyrobot.model.util

import com.test.toyrobot.domain.model.CardinalDirection
import com.test.toyrobot.domain.model.Position
import com.test.toyrobot.domain.model.TableSize
import com.test.toyrobot.domain.util.PositionUtil
import com.test.toyrobot.domain.util.toCardinalDirectionOrNull
import org.junit.Assert
import org.junit.Test

class StringExtensionsTest {

    @Test
    fun toCardinalDirectionOrNull_returnsExpected() {
        Assert.assertEquals(null, "A".toCardinalDirectionOrNull())

        Assert.assertEquals(CardinalDirection.NORTH, "n".toCardinalDirectionOrNull())
        Assert.assertEquals(CardinalDirection.NORTH, "N".toCardinalDirectionOrNull())
        Assert.assertEquals(CardinalDirection.NORTH, "noRth".toCardinalDirectionOrNull())

        Assert.assertEquals(CardinalDirection.EAST, "e".toCardinalDirectionOrNull())
        Assert.assertEquals(CardinalDirection.EAST, "E".toCardinalDirectionOrNull())
        Assert.assertEquals(CardinalDirection.EAST, "East".toCardinalDirectionOrNull())

        Assert.assertEquals(CardinalDirection.SOUTH, "s".toCardinalDirectionOrNull())
        Assert.assertEquals(CardinalDirection.SOUTH, "S".toCardinalDirectionOrNull())
        Assert.assertEquals(CardinalDirection.SOUTH, "South".toCardinalDirectionOrNull())

        Assert.assertEquals(CardinalDirection.WEST, "w".toCardinalDirectionOrNull())
        Assert.assertEquals(CardinalDirection.WEST, "W".toCardinalDirectionOrNull())
        Assert.assertEquals(CardinalDirection.WEST, "weST".toCardinalDirectionOrNull())

    }

}